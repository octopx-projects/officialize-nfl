# Officialize NFL Project - Odell Beckham Jr.

This project is a responsive single page website design and development to highlight life and career of NFL New York Giants' player [Odell Beckham Jr](https://www.odellbeckhamjr13.com/). and his brand, creating a pleasant and inspiring experience to NFL fans.

The requirements of this project are:   
	* Creating a multiple screen design;  
	* Developing a CSS-driven HTML5 website;  
	* Using Javascript code to feature detection, user interaction and visual improvement;   
	* Building a graphically rich interative timeline about the athlete's career;   
	* Creating a 45 seconds to 1 minute promotional video heavily influenced by 3D design to present Beckham's highlights;  
	* Developing custom video player controls;  
	* Following best practices of accessibility, UX/UI and progressive enhancement.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

At this point of first phase the project team is still researching and defining tools and technologies to produce the campaign, including the website. However, it is very likely that tools like SASS will be used in this project. Along the research and development, if some technology is included as prerequisite to develop, deploy or run this project, this README file will be properly updated.

### Prerequisites

**Git:**
To clone this project to a local repository, it is required using [Git](https://git-scm.com/). Follow instructions provided [here](https://git-scm.com/downloads) to install and configure Git.

**NPM:**    
To develop this project, it is required installing and running [NPM](https://www.npmjs.com/), and consequently [NodeJS](https://nodejs.org/en/). Follow instructions provided [here](https://nodejs.org/en/download/) to install and configure properly NodeJS and NPM in your machine.

```
sass --watch <your work path>/ledc/scss:<your work path>/ledc/css
```

## Deployment

After project cloned and all prerequisites installed, it is necessary running the command below in the root directory to deploy properly all development dependencies of this project:

```
npm install
```

Grunt is implemented as a task runner for this project. Grunt tasks include: concatenation and js uglify; run sass; and autoprefixer through postcss. Grunt watch for changes in js and all scss files. In order to watch changes made in the files, run grunt inside the project folder by the command below:

```
grunt
```

## Contributing

At this moment of this project, due to the fact that this is a school project yet, we are not allowed to receive or accept any kind of intelectual or development contribution, though we are aware that exchange of ideas and work always improve code and projects.

## Versioning

For the versions available, see the [tags on this repository](https://bitbucket.org/octopx-projects/ledc).

## Authors

[**Octopx Digital**](https://bitbucket.org/octopx-projects/) is:  
	* [**Barbara Bombachini**](https://bitbucket.org/bbombachini/) - Backend Developer  
	* [**Emre Filiz**](https://bitbucket.org/emrefiliz/) - Graphic Designer  
	* [**Eric Lee**](https://bitbucket.org/elee378/) - Motion/3D Artist  
	* [**Flavia Tozzini**](https://bitbucket.org/f-tozzini/) - Frontend Developer  
	* [**Mauricio Silveira**](https://bitbucket.org/maursilveira/) - Project Manager  


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
